import { Component, OnInit } from '@angular/core';
import { Product } from '../product';
import { PRODUCTS } from '../mock-products';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products: Product[] = PRODUCTS
  selectedProduct: Product
  selectedIndex

  constructor() { }

  ngOnInit() {
  }

  onSelect(product: Product, index) {
    var tempProduct = new Product()
    tempProduct.id = product.id
    tempProduct.name = product.name
    tempProduct.price = product.price
    tempProduct.descr = product.descr

    this.selectedProduct = tempProduct
    this.selectedIndex = index
    console.log(this.selectedIndex)
  }

  delete(index){
    this.products.splice(index,1)
  }

  save(){
    this.products.splice(this.selectedIndex,1,this.selectedProduct)
  }
  add() {
    var tempProduct = new Product()
    this.selectedProduct = tempProduct
    this.selectedIndex = this.products.length

  }

}
