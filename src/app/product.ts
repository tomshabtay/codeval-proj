export class Product {
    id: number;
    name: string;
    descr: string;
    price: number;
  }