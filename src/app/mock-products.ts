import { Product} from './product';

export const PRODUCTS: Product[] = [
  { id: 11, name: 'Milk', descr: "really good!" ,price: 10 },
  { id: 12, name: 'Pizza' , descr: "really good!" ,price: 10},
  { id: 13, name: 'Cola' , descr: "really good!" ,price: 10},
  { id: 14, name: 'Watermelon' , descr: "really good!" ,price: 10},
  { id: 15, name: 'Avocado' , descr: "really good!" ,price: 10}
];